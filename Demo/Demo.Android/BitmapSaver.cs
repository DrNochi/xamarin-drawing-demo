﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(Demo.Droid.BitmapSaver))]

namespace Demo.Droid
{
    class BitmapSaver : IBitmapSaver
    {
        public async Task<bool> SaveImage(byte[] bitmapData, string filename)
        {
            try
            {
                File picturesFolder = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures);
                File appFolder = new File(picturesFolder, "DemoMalApp");
                appFolder.Mkdir();

                using (File bitmapFile = new File(appFolder, filename))
                {
                    bitmapFile.CreateNewFile();

                    using (FileOutputStream outputStream = new FileOutputStream(bitmapFile))
                    {
                        await outputStream.WriteAsync(bitmapData);
                    }

                    MediaScannerConnection.ScanFile(MainActivity.Context, new string[] { bitmapFile.Path }, new string[] { "image/png", "image/jpeg" }, null);
                }
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}