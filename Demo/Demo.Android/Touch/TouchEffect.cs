﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("Touch")]
[assembly: ExportEffect(typeof(Touch.Droid.TouchEffect), "TouchEffect")]

namespace Touch.Droid
{
    class TouchEffect : PlatformEffect
    {
        Android.Views.View platformView;
        Touch.TouchEffect effect;

        protected override void OnAttached()
        {
            platformView = Control == null ? Container : Control;

            effect = (Touch.TouchEffect)Element.Effects.FirstOrDefault(e => e is Touch.TouchEffect);

            if (effect != null && platformView != null)
            {
                platformView.Touch += OnTouch;
            }
        }

        protected override void OnDetached()
        {
            if (effect != null && platformView != null)
            {
                platformView.Touch -= OnTouch;
            }
        }

        public void OnTouch(object sender, Android.Views.View.TouchEventArgs e)
        {
            switch (e.Event.ActionMasked)
            {
                case MotionEventActions.Down:
                case MotionEventActions.PointerDown:
                case MotionEventActions.ButtonPress:
                    Point point = new Point(platformView.Context.FromPixels(e.Event.GetX()), platformView.Context.FromPixels(e.Event.GetY()));
                    effect.OnTouched(Element, new TouchEventArgs(e.Event.GetPointerId(e.Event.ActionIndex), TouchEventType.Pressed, point, true));
                    break;

                case MotionEventActions.Move:
                    point = new Point(platformView.Context.FromPixels(e.Event.GetX()), platformView.Context.FromPixels(e.Event.GetY()));
                    effect.OnTouched(Element, new TouchEventArgs(e.Event.GetPointerId(e.Event.ActionIndex), TouchEventType.Moved, point, true));
                    break;

                case MotionEventActions.Up:
                case MotionEventActions.PointerUp:
                case MotionEventActions.ButtonRelease:
                    point = new Point(platformView.Context.FromPixels(e.Event.GetX()), platformView.Context.FromPixels(e.Event.GetY()));
                    effect.OnTouched(Element, new TouchEventArgs(e.Event.GetPointerId(e.Event.ActionIndex), TouchEventType.Released, point, true));
                    break;
            }
        }
    }
}