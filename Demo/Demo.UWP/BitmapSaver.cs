﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

[assembly: Xamarin.Forms.Dependency(typeof(Demo.UWP.BitmapSaver))]

namespace Demo.UWP
{
    class BitmapSaver : IBitmapSaver
    {
        public async Task<bool> SaveImage(byte[] bitmapData, string filename)
        {
            StorageFolder picturesFolder = KnownFolders.PicturesLibrary;
            StorageFolder appFolder = null;

            try
            {
                appFolder = await picturesFolder.GetFolderAsync("DemoMalApp");
            }
            catch { }

            if (appFolder == null)
            {
                try
                {
                    appFolder = await picturesFolder.CreateFolderAsync("DemoMalApp");
                }
                catch
                {
                    return false;
                }
            }

            try
            {
                StorageFile bitmapFile = await appFolder.CreateFileAsync(filename);
                await FileIO.WriteBytesAsync(bitmapFile, bitmapData);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
