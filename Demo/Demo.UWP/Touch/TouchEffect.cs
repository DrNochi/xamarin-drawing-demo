﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ResolutionGroupName("Touch")]
[assembly: ExportEffect(typeof(Touch.UWP.TouchEffect), "TouchEffect")]

namespace Touch.UWP
{
    class TouchEffect : PlatformEffect
    {
        FrameworkElement platformElement;
        Touch.TouchEffect effect;

        protected override void OnAttached()
        {
            platformElement = Control == null ? Container : Control;

            effect = (Touch.TouchEffect)Element.Effects.FirstOrDefault(e => e is Touch.TouchEffect);

            if (effect != null && platformElement != null)
            {
                platformElement.PointerPressed += OnPointerPressed;
                platformElement.PointerMoved += OnPointerMoved;
                platformElement.PointerReleased += OnPointerReleased;
            }
        }

        protected override void OnDetached()
        {
            if (effect != null && platformElement != null)
            {
                platformElement.PointerPressed -= OnPointerPressed;
                platformElement.PointerMoved -= OnPointerMoved;
                platformElement.PointerReleased -= OnPointerReleased;
            }
        }

        void OnPointerPressed(object sender, PointerRoutedEventArgs e)
        {
            Windows.Foundation.Point platformPoint = e.GetCurrentPoint(sender as UIElement).Position;
            effect.OnTouched(Element, new TouchEventArgs(e.Pointer.PointerId, TouchEventType.Pressed, new Point(platformPoint.X, platformPoint.Y), e.Pointer.IsInContact));
        }

        void OnPointerMoved(object sender, PointerRoutedEventArgs e)
        {
            Windows.Foundation.Point platformPoint = e.GetCurrentPoint(sender as UIElement).Position;
            effect.OnTouched(Element, new TouchEventArgs(e.Pointer.PointerId, TouchEventType.Moved, new Point(platformPoint.X, platformPoint.Y), e.Pointer.IsInContact));
        }

        void OnPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            Windows.Foundation.Point platformPoint = e.GetCurrentPoint(sender as UIElement).Position;
            effect.OnTouched(Element, new TouchEventArgs(e.Pointer.PointerId, TouchEventType.Released, new Point(platformPoint.X, platformPoint.Y), e.Pointer.IsInContact));
        }
    }
}
