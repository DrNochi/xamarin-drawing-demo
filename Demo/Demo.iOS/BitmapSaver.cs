﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(Demo.iOS.BitmapSaver))]

namespace Demo.iOS
{
    class BitmapSaver : IBitmapSaver
    {
        public Task<bool> SaveImage(byte[] bitmapData, string filename)
        {
            NSData data = NSData.FromArray(bitmapData);
            UIImage image = new UIImage(data);

            TaskCompletionSource<bool> taskCompletionSource = new TaskCompletionSource<bool>();

            image.SaveToPhotosAlbum((UIImage img, NSError error) =>
            {
                taskCompletionSource.SetResult(error == null);
            });

            return taskCompletionSource.Task;
        }
    }
}
