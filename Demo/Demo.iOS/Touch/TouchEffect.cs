﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ResolutionGroupName("Touch")]
[assembly: ExportEffect(typeof(Touch.iOS.TouchEffect), "TouchEffect")]

namespace Touch.iOS
{
    class TouchEffect : PlatformEffect
    {
        UIView platformView;
        Touch.TouchEffect effect;
        TouchGestureRecognizer touchGestureRecognizer;

        protected override void OnAttached()
        {
            platformView = Control == null ? Container : Control;

            effect = (Touch.TouchEffect)Element.Effects.FirstOrDefault(e => e is Touch.TouchEffect);

            if (effect != null && platformView != null)
            {
                touchGestureRecognizer = new TouchGestureRecognizer(Element, platformView, effect);
                platformView.AddGestureRecognizer(touchGestureRecognizer);
            }
        }

        protected override void OnDetached()
        {
            if (effect != null && platformView != null)
            {
                platformView.RemoveGestureRecognizer(touchGestureRecognizer);
            }
        }
    }
}