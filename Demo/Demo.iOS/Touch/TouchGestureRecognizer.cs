﻿using System;
using System.Collections.Generic;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;
using Xamarin.Forms;

namespace Touch.iOS
{
    class TouchGestureRecognizer : UIGestureRecognizer
    {
        Element element;
        UIView platformView;
        Touch.TouchEffect effect;

        public TouchGestureRecognizer(Element formsElement, UIView view, Touch.TouchEffect touchEffect)
        {
            element = formsElement;
            platformView = view;
            effect = touchEffect;
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);

            foreach (UITouch touch in touches)
            {
                CGPoint platformPoint = touch.LocationInView(platformView);

                effect.OnTouched(element, new TouchEventArgs(touch.Handle.ToInt64(), TouchEventType.Pressed, new Point(platformPoint.X, platformPoint.Y), true));
            }
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);

            foreach (UITouch touch in touches)
            {
                CGPoint platformPoint = touch.LocationInView(platformView);

                effect.OnTouched(element, new TouchEventArgs(touch.Handle.ToInt64(), TouchEventType.Pressed, new Point(platformPoint.X, platformPoint.Y), true));
            }
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);

            foreach (UITouch touch in touches)
            {
                CGPoint platformPoint = touch.LocationInView(platformView);

                effect.OnTouched(element, new TouchEventArgs(touch.Handle.ToInt64(), TouchEventType.Pressed, new Point(platformPoint.X, platformPoint.Y), true));
            }
        }
    }
}
