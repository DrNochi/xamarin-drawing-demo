﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    public interface IBitmapSaver
    {
        Task<bool> SaveImage(byte[] bitmapData, string filename);
    }
}
