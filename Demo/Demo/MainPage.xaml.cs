﻿using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Touch;
using Xamarin.Forms;

namespace Demo
{
    class TouchInfo
    {
        public Point Position;
        public Point LastPosition;
    }

    public partial class MainPage : ContentPage
	{
        SKCanvasView canvasView;
        SKBitmap image;
        SKCanvas canvas;
        SKPaint style = new SKPaint { Style = SKPaintStyle.Stroke, StrokeCap = SKStrokeCap.Round, Color = SKColors.Black };

        Dictionary<long, TouchInfo> touches = new Dictionary<long, TouchInfo>();

        bool randomColor;
        bool clear = true;
        bool copyCanvasToImage;

        Stopwatch timer = new Stopwatch();
        bool rotate;
        float angle;

        bool needSave = false;

        public MainPage()
		{
			InitializeComponent();

            canvasView = new SKCanvasView();
            canvasView.PaintSurface += OnCanvasPaintSurface;
            canvasView.Effects.Add(new TouchEffect(OnCanvasTouched));
            canvasContainer.Content = canvasView;

            timer.Start();
            Device.StartTimer(TimeSpan.FromMilliseconds(0), OnTick);
        }

        bool OnTick()
        {
            float tColor = timer.ElapsedMilliseconds % 10000 / 10000f;
            randomColorButton.BackgroundColor = Color.FromHsla(360 * tColor / 360, 1, 0.5);

            if (image == null)
            {
                return true;
            }

            if (randomColor)
            {
                style.Color = randomColorButton.BackgroundColor.ToSKColor();
            }

            if (rotate)
            {
                float tAngle = timer.ElapsedMilliseconds % 5000 / 5000f;
                angle = 360 * tAngle;
            }

            style.StrokeWidth = (float)widthSlider.Value;

            SKMatrix matrix = SKMatrix.MakeRotationDegrees(-angle, image.Width / 2, image.Height / 2);

            foreach (TouchInfo touch in touches.Values)
            {
                SKPoint point = touch.Position.ToSKPoint();
                float scaleX = (float)(canvasView.CanvasSize.Width / canvasView.Width);
                float scaleY = (float)(canvasView.CanvasSize.Height / canvasView.Height);
                point.X *= scaleX;
                point.Y *= scaleY;
                point = matrix.MapPoint(point);

                if (!double.IsNaN(touch.LastPosition.X) && !double.IsNaN(touch.LastPosition.Y))
                {
                    canvas.DrawLine((float)touch.LastPosition.X, (float)touch.LastPosition.Y, point.X, point.Y, style);
                }

                touch.LastPosition = point.ToFormsPoint();
            }

            canvasView.InvalidateSurface();

            return true;
        }

        void OnCanvasTouched(object sender, TouchEventArgs e)
        {
            switch (e.Type)
            {
                case TouchEventType.Pressed:
                    if (!touches.ContainsKey(e.Id))
                    {
                        touches.Add(e.Id, new TouchInfo { Position = e.Location, LastPosition = new Point(double.NaN, double.NaN) });
                    }
                    break;

                case TouchEventType.Moved:
                    if (touches.ContainsKey(e.Id))
                    {
                        touches[e.Id].Position = e.Location;

                        //OnTick();
                    }
                    break;

                case TouchEventType.Released:
                    touches.Remove(e.Id);
                    break;
            }
        }

        void OnCanvasPaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            if (image == null)
            {
                image = new SKBitmap((int)canvasView.CanvasSize.Width, (int)canvasView.CanvasSize.Height);
                canvas = new SKCanvas(image);
            }

            if (copyCanvasToImage)
            {
                image = SKBitmap.FromImage(e.Surface.Snapshot());
                canvas = new SKCanvas(image);
                copyCanvasToImage = false;
            }

            if (image.Width != canvasView.CanvasSize.Width || image.Height != canvasView.CanvasSize.Height)
            {
                image = image.Resize(new SKImageInfo((int)canvasView.CanvasSize.Width, (int)canvasView.CanvasSize.Height), SKBitmapResizeMethod.Lanczos3);
                canvas = new SKCanvas(image);
            }

            if (clear)
            {
                e.Surface.Canvas.Clear();
            }

            e.Surface.Canvas.RotateDegrees(angle, image.Width / 2, image.Height / 2);

            e.Surface.Canvas.DrawBitmap(image, 0, 0);
        }

        void OnColorPicked(object sender, EventArgs e)
        {
            randomColor = false;
            randomColorButton.Text = "Aus";
            style.Color = ((VisualElement)sender).BackgroundColor.ToSKColor();
        }

        void OnRandomColor(object sender, EventArgs e)
        {
            randomColor = !randomColor;

            if (randomColor)
            {
                randomColorButton.Text = "An";
            }
            else
            {
                randomColorButton.Text = "Aus";
            }
        }

        void OnEffectClicked(object sender, EventArgs e)
        {
            Random random = new Random();
            SKImageFilter effect = null;

            switch (random.Next(4))
            {
                case 0:
                    effect = SKImageFilter.CreateBlur((float)Math.Pow(random.NextDouble() * 5, 2), (float)Math.Pow(random.NextDouble() * 5, 2));
                    break;

                case 1:
                    float[] filter = new float[SKColorFilter.ColorMatrixSize];

                    for (int i = 0; i < filter.Length; i++)
                    {
                        filter[i] = (float)Math.Pow(random.NextDouble(), 2);
                    }

                    effect = SKImageFilter.CreateColorFilter(SKColorFilter.CreateColorMatrix(filter));
                    break;

                case 2:
                    effect = SKImageFilter.CreateDilate((int)Math.Pow(random.NextDouble() * 5, 2), (int)Math.Pow(random.NextDouble() * 5, 2));
                    break;

                case 3:
                    effect = SKImageFilter.CreateErode((int)Math.Pow(random.NextDouble() * 5, 2), (int)Math.Pow(random.NextDouble() * 5, 2));
                    break;
            }

            if (effect != null)
            {
                SKImage tmpImage = SKImage.FromBitmap(image);
                SKRectI outSubset;
                SKPoint outOffset;
                SKImage filteredImage = tmpImage.ApplyImageFilter(effect, SKRectI.Create(0, 0, tmpImage.Width, tmpImage.Height), SKRectI.Create(0, 0, tmpImage.Width, tmpImage.Height), out outSubset, out outOffset);

                if (filteredImage != null)
                {
                    if (clear)
                    {
                        canvas.Clear();
                    }

                    canvas.DrawImage(filteredImage, SKRect.Create(0, 0, image.Width, image.Height));
                }
            }
        }

        void OnSaveClicked(object sender, EventArgs e)
        {
            needSave = true;
            canvasView.PaintSurface += SaveHandler;
            canvasView.InvalidateSurface();
        }

        async void SaveHandler(object sender, SKPaintSurfaceEventArgs e)
        {
            if (needSave)
            {
                needSave = false;

                DateTime timestamp = DateTime.Now;
                string filename = String.Format("DemoMalApp-{0:D4}{1:D2}{2:D2}-{3:D2}{4:D2}{5:D2}{6:D3}.png",
                                                timestamp.Year, timestamp.Month, timestamp.Day, timestamp.Hour, timestamp.Minute, timestamp.Second, timestamp.Millisecond);

                if (await DependencyService.Get<IBitmapSaver>().SaveImage(e.Surface.Snapshot().Encode().ToArray(), filename))
                {
                    await DisplayAlert("Gespeichert!", "Das Bild wurde erfolgreich gespeichert :)", "Ok");
                }
                else
                {
                    await DisplayAlert("Speichern fehlgeschlagen!", "Das Bild konnte nicht gespeichert werden :/", "Ok");
                }
            }

            canvasView.PaintSurface -= SaveHandler;
        }

        void OnClearClicked(object sender, EventArgs e)
        {
            canvas.Clear();

            randomColor = false;
            rotate = false;
            clear = true;

            randomColorButton.Text = "Aus";
            RotateButton.FontAttributes = FontAttributes.None;
            DaubButton.FontAttributes = FontAttributes.None;
        }

        void OnRotateClicked(object sender, EventArgs e)
        {
            rotate = !rotate;

            if (rotate)
            {
                RotateButton.FontAttributes = FontAttributes.Bold;
            }
            else
            {
                RotateButton.FontAttributes = FontAttributes.None;

                if (!clear)
                {
                    copyCanvasToImage = true;
                }
            }

            timer.Restart();
            angle = 0;
        }

        void OnDaubClicked(object sender, EventArgs e)
        {
            clear = !clear;

            if (clear)
            {
                DaubButton.FontAttributes = FontAttributes.None;
            }
            else
            {
                DaubButton.FontAttributes = FontAttributes.Bold;
            }
        }
    }
}
