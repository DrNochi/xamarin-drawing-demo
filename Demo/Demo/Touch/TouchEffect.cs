﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Touch
{
    public enum TouchEventType
    {
        Pressed,
        Moved,
        Released
    }

    public class TouchEffect : RoutingEffect
    {
        public event EventHandler<TouchEventArgs> Touched;

        public TouchEffect(EventHandler<TouchEventArgs> touchEventHandler) : base("Touch.TouchEffect")
        {
            Touched += touchEventHandler;
        }

        public void OnTouched(Element sender, TouchEventArgs e)
        {
            Touched(sender, e);
        }
    }

    public class TouchEventArgs : EventArgs
    {
        public long Id { get; private set; }
        public TouchEventType Type { get; private set; }
        public Point Location { get; private set; }
        public bool IsInContatct { get; private set; }

        public TouchEventArgs(long id, TouchEventType type, Point location, bool inContact)
        {
            Id = id;
            Type = type;
            Location = location;
            IsInContatct = inContact;
        }
    }
}
